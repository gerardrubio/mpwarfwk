<?php
/**
 * Created by PhpStorm.
 * User: grubio
 * Date: 3/9/15
 * Time: 8:46 PM
 */

namespace gerardrubio\mpwarfwk;

interface IResponse
{
    /**
     * @param $data mixed: Data to output
     */
    public function __construct($data);

    /***
     * @return mixed
     */
    public function send();
}
