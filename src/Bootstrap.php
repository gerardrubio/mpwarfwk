<?php
/**
 * Created by PhpStorm.
 * User: grubio
 * Date: 3/9/15
 * Time: 8:42 PM
 */

namespace gerardrubio\mpwarfwk;

class Bootstrap
{
    protected $_routes = [];

    public function __construct()
    {
        $this->register('/', function()
        {
            return new Response("Hello world");
        });
    }

    protected function routeIsDefined($route)
    {
        return array_key_exists($route, $this->_routes);
    }

    /**
     * @param IRequest $request
     * @return Response
     */
    public function handle(IRequest $request)
    {
        $requestUrl = $request->url();
        if ($this->routeIsDefined($requestUrl))
        {
            return $this->_routes[$requestUrl]();
        }

        throw new RouteNotFoundException;
    }

    public function register($url, $callback)
    {
        if (!$this->routeIsDefined($url))
        {
            $this->_routes[$url] = $callback;
            return;
        }

        throw new RouteAlreadyDefinedException;
    }
}
