<?php
/**
 * Created by PhpStorm.
 * User: grubio
 * Date: 3/9/15
 * Time: 8:47 PM
 */

namespace gerardrubio\mpwarfwk;

interface IRequest
{
    public static function fromGlobals();
    public function url();
}
