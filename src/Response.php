<?php

namespace gerardrubio\mpwarfwk;

class Response implements IResponse
{
    protected $_data = null;

    public function __construct($data)
    {
        $this->_data = $data;
    }

    public function send()
    {
        echo $this->_data;
    }
}
