<?php

namespace gerardrubio\mpwarfwk;

class Request implements IRequest
{
    protected $_params;
    protected $_url;

    protected function __construct()
    {
        $this->_url = $_SERVER['REQUEST_URI'];
        $this->_params = [
            'get' => $_GET,
            'post' => $_POST,
            'files' => $_FILES
        ];
    }

    public function input($property, $method = 'get')
    {
        if (array_key_exists($method, $this->_params))
        {
            if (array_key_exists($property, $this->_params[$method]))
                return $this->_params[$method][$property];
        }

        throw new InputArgumentNotDefinedException;
    }

    public function url()
    {
        return $this->_url;
    }

    public static function fromGlobals()
    {
        return new Request;
    }
}
